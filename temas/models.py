from django.db import models

class Temas(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField()
    relevancia = models.IntegerField()

    def __str__(self):
        return self.nome
