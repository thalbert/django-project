from rest_framework.serializers import ModelSerializer
from temas.models import Temas

class TemasSerializer(ModelSerializer):
    class Meta:
        model = Temas
        fields = ('id', 'nome', 'descricao' ,'relevancia')
        # fields = ['id', 'nome', 'descricao', 'aprovado', 'temas', 'comentarios', 'avaliacoes', 'enditora']
