from rest_framework.viewsets import ModelViewSet
from temas.models import Temas
from .serializers import TemasSerializer

class TemaViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Temas.objects.all()
    serializer_class = TemasSerializer