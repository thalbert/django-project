from django.apps import AppConfig


class EditorasConfig(AppConfig):
    name = 'editoras'
