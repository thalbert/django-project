from rest_framework.serializers import ModelSerializer
from quickstart.models import Editoras

class EditorasSerializer(ModelSerializer):
    class Meta:
        model = Editoras
        fields = ('id', 'nome', 'sede')
        # fields = ['id', 'nome', 'descricao', 'aprovado', 'temas', 'comentarios', 'avaliacoes', 'enditora']

