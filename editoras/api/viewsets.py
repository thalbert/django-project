from rest_framework.viewsets import ModelViewSet
from quickstart.models import Editoras
from .serializers import EditorasSerializer


class EditorasViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Editoras.objects.all()
    serializer_class = EditorasSerializer