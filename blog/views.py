from django.shortcuts import render
from django.http import HttpResponse

posts = [
    {
        'autor' : 'Thalbert',
        'titulo' : 'Livros',
        'coteudo' : 'Cristianismo Puro e Simples',
        'data' : '02 de Janeiro'
    },
    {
        'autor' : 'Wesley',
        'titulo' : 'Devocionais',
        'coteudo' : 'O verdadeiro Evangelho',
        'data' : '12 de Novembro'
    }
]

def home(request):
    context = {
        'posts' : posts
    }
    return render(request, 'blog/home.html', context)

def about(request):
    return render(request, 'blog/about.html')