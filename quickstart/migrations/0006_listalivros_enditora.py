# Generated by Django 3.0.3 on 2020-02-15 01:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('editoras', '0001_initial'),
        ('quickstart', '0005_listalivros_avaliacoes'),
    ]

    operations = [
        migrations.AddField(
            model_name='listalivros',
            name='enditora',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='editoras.Editoras'),
            preserve_default=False,
        ),
    ]
