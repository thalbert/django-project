from django.db import models
from temas.models import Temas
from comentarios.models import Comentario
from avaliacoes.models import Avaliacao
from editoras.models import Editoras


class ListaLivros(models.Model):
    nome = models.CharField(max_length=150)
    descricao = models.TextField()
    aprovado = models.BooleanField(default=False)
    temas = models.ManyToManyField(Temas)
    comentarios = models.ManyToManyField(Comentario)
    avaliacoes = models.ManyToManyField(Avaliacao)
    enditora = models.ForeignKey(Editoras, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nome