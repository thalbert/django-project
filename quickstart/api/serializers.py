from rest_framework.serializers import ModelSerializer
from quickstart.models import ListaLivros

class ListaLivrosSerializer(ModelSerializer):
    class Meta:
        model = ListaLivros
        fields = ('id', 'nome', 'descricao')
        # fields = ['id', 'nome', 'descricao', 'aprovado', 'temas', 'comentarios', 'avaliacoes', 'enditora']




