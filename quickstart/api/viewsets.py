from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from quickstart.models import ListaLivros
from .serializers import ListaLivrosSerializer

class ListaLivrosViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = ListaLivros.objects.all()
    serializer_class = ListaLivrosSerializer